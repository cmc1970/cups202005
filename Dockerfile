FROM debian:buster-slim 

ARG DEBIAN_FRONTEND=noninteractive

RUN \
    apt update && apt install -yq  \
    cups \
    && apt clean
